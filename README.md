# Dockage

**DRAFT: Dynamic Packaging tool:** _Dockage because it reminds of "Package"_

## Vision

To ease packaging and deployment of microservices.

I got the idea originally from other great projects:

* Paket and how they manage dependencies in a monorepo.
* Lerna and Bolt and how they manage dependencies in a monorepo
* Terraform and how their idea of computing and displaying a "plan" before applying changes.
* Habitat and their vision
  * https://www.habitat.sh/docs/diagrams/

## Development

### Platforms

* Git
* Docker + Docker Repository
* Kubernetes
* Swarm
* ?

### Versioning / Labeling

A `VERSION` file is in the root of the project and specifies the version of the project

## Runtime

### Commands

#### plan

Shows the computed plan (inspired by `terraform plan`)

#### apply

Applies plan _(see algorithm below)_

### Algorithm

Run Dockage in a Docker container and outputs a microservice deployment file (Docker Compose, Kubernetes, etc.):

```
docker build -t dockage . && docker run --rm dockage > docker-compose.yml
```

#### Output / Artifact

* docker-compose or Kubernetes Deployment file ?
* How about Docker images?

### **WIP: Notes / Thoughts / Misc**

#### Thoughts / Decisions / Planning

##### Programming language detection phase

https://github.com/github/linguist

##### Programming language?

* C# .NET Core?
* Node + TypeScript ?

##### Environments 

Test, Staging, Release, Live
Keep it dynamic, expandable

##### Namespaces (for each project)

Multiple connected projects. Think maybe of *modules* (like Terraform does) or *workspaces* like Bolt/VSCode do?

##### Configuration


YAML files use k8s header syntax

Parsing of YAML: https://github.com/nodeca/js-yaml

Example:

```
apiVersion: dockage.v1
kind: Module
```

* `dockage.yaml` file
  * Use [cosmiconfig](https://github.com/davidtheclark/cosmiconfig) to read configuration files.
  * Root file contains:
    * Header kind RootModule
    * Configuration
    * List of all modules
  * Module header information
    * `name = sample-app`
    * `namespace = your_origin`
    * `version = "1.0.1"`
    * One YAML file in each "module" root `dockage.yaml` that contains:
      * Other modules it depends on
        * How should that be?
          * Maybe the parent directory is the name of the module?
      * How a module should be built, tested, packaged and deployed
      * "dockerignore" section that creates a `.dockerignore` file before building Docker images


##### Kinds

* RootModule
* Module
  * Multiple projects that are connected, and should be managed together as a cohesive unit

##### Dependency graph / Directed Acyclic Graph

* Directory tree based structure
* From bottom-up, the top-most `dockage.yaml` includes all files below (his zone)
* Based on diff and changed files, we compute a dependency graph (GraphViz and DOT syntax: https://www.graphviz.org)

**Diff**

Changed files in last git diff (hash1..hash2), in stdin. This makes it independent of git


##### Schema standardized labeling

Standardized labeling of Docker images

Label Schema: http://label-schema.org/rc1/

Example from Traefik project:

```
"Labels": {
	"org.label-schema.description": "A modern reverse-proxy",
	"org.label-schema.docker.schema-version": "1.0",
	"org.label-schema.name": "Traefik",
	"org.label-schema.url": "https://traefik.io",
	"org.label-schema.vendor": "Containous",
	"org.label-schema.version": "v1.6.0-rc1"
}
```

Example from Docker Swarm:

```
 "Labels": {
  "com.docker.stack.namespace": "sdc-prod",
  "com.docker.swarm.node.id": "p34h6xa5bpyn1ukdpkic91nfi",
  "com.docker.swarm.service.id": "fnh1qigr6fkiccm3auoh9umsk",
  "com.docker.swarm.service.name": "sdc-prod_api",
  "com.docker.swarm.task": "",
  "com.docker.swarm.task.id": "qaghcawwkxkx4g0lp58b11kwz",
  "com.docker.swarm.task.name": "sdc-prod_api.1.qaghcawwkxkx4g0lp58b11kwz"
}
```

#### Remember

* Remember to be able to provide `/.paket` folder or something other, just a "external dependency" you want into your
  build context.
* Be able to set an organization for the tagged Docker image:
  * Registry = registry.domain.com
  * Organization = company/organization
  * Project name = project_name
  * Tag = latest
  * Full: `registry.domain.com/company/organization/project_name:latest`

#### Triggers

_(Docker Hub automated builds)_

* Type
  * Branch
  * Tag
* Name
  * master or regex pattern /.\*/
* Dockerfile location
* Produced Docker tag name (latest)
